# Rpg App (frontend)

Rpg App purpose is to make playing Warhammer Fantasy Role-play easier. The desired features:

* Creating a new game as a master, then inviting other users to be players
* Each player can create one character per game
* During gameplay all players can see changes master makes to their characters in real time
* Master can change all characters' properties, add gold and experience points
* players can upgrade their characters