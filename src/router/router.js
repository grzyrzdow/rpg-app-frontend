import VueRouter from "vue-router";
import Vue from "vue";
import store from "@/stores/store";

import Login from "@/components/auth/Login";
import Register from "@/components/auth/Register";
import Error404 from "@/components/errors/Error404";
import Games from "@/components/common/Games";
import NewGame from "@/components/common/NewGame";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "homepage",
    meta: {
      requiresAuth: false,
      title: "Strona główna"
    }
  },
  {
    path: "/login",
    component: Login,
    props: route => ({
      alertError: !!route.query.redirect,
      alertMessage: !!route.query.redirect ? "Zaloguj się, by kontynuować" : ""
    }),
    name: "login",
    meta: {
      requiresAuth: false,
      title: "Logowanie",
      transition: "auth-trans"
    }
  },
  {
    path: "/register",
    component: Register,
    name: "register",
    meta: {
      requiresAuth: false,
      title: "Rejestracja",
      transition: "auth-trans"
    }
  },
  {
    path: "/games",
    component: Games,
    name: "games",
    meta: {
      requiresAuth: true,
      title: "Twoje gry"
    },
    children: [
      {
        path: "new",
        component: NewGame,
        name: "newGame",
        meta: {
          requiresAuth: true,
          title: "Nowa gra"
        }
      }
    ]
  },
  {
    path: "*",
    component: Error404,
    name: "error404",
    meta: {
      title: "404"
    }
  }
];

const router = new VueRouter({
  mode: "history",
  routes,
  base: "/"
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth && !store.getters.isAuthenticated) {
    next({
      path: "/login",
      query: {
        redirect: to.path
      }
    });
  } else if (
    (to.name === "login" || to.name === "register") &&
    store.getters.isAuthenticated
  ) {
    next(from);
  } else {
    next();
  }
});
router.afterEach((to, from) => {
  if (!!to.meta.title) {
    document.title = to.meta.title + " | Boros112 RPG";
  } else {
    document.title = "Boros112 RPG";
  }
});
export default router;
