import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import "vuetify/dist/vuetify.min.css";
import "./assets/font/fontDef.css";
import store from "./stores/store";
import router from "./router/router";
import axios from "axios";

axios.defaults.baseURL = "http://localhost:8000/api/";
axios.defaults.withCredentials = true;

axios.interceptors.request.use(config => {
  config.headers.authorization = "Bearer " + store.getters.getToken;
  return config;
});
axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if (error.response.status === 401 && router.currentRoute.name !== "login") {
      store.dispatch("logout", "");
      router.replace({
        path: "/login",
        query: {
          redirect: router.currentRoute.path
        }
      });
    }
    return Promise.reject(error);
  }
);

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount("#app");
