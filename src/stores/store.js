import Vue from "vue";
import Vuex from "vuex";
import Cookie from "js-cookie";
import VuexPersistence from "vuex-persist";

import User from "./modules/user";
import Ui from "./modules/ui";
import Games from "./modules/games";

Vue.use(Vuex);

const userCookie = new VuexPersistence({
  key: "userCookie",
  restoreState: (key, storage) => Cookie.getJSON(key),
  saveState: (key, state, storage) => {
    Cookie.set(key, state, {
      expires: 1
    });
  },
  modules: ["user"]
});

export default new Vuex.Store({
  modules: {
    user: User,
    ui: Ui,
    games: Games
  },
  plugins: [userCookie.plugin]
});
