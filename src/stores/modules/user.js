import axios from "axios";
import Cookie from "js-cookie";

export default {
  state: {
    token: "",
    user: {
      id: "",
      username: "",
      roles: []
    }
  },
  getters: {
    getToken: state => {
      return state.token;
    },
    isAuthenticated: state => {
      return !!state.token;
    },
    getUser: state => {
      return state.user;
    }
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_USER: (state, user) => {
      state.user = user;
    }
  },
  actions: {
    login: ({ commit, dispatch }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post("login_check", payload)
          .then(({ data, status }) => {
            if (status === 200) {
              var token = data.token;
              commit("SET_TOKEN", token);
              Cookie.set("token", token);
              dispatch("setCurrentUser")
                .then(success => {
                  resolve();
                })
                .catch(error => {
                  reject(error);
                });
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    register: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post("register", payload)
          .then(({ data, status }) => {
            if (status === 201) {
              var token = data.token;
              commit("SET_TOKEN", token);
              Cookie.set("token", token);
              commit("SET_USER", data.user);
              resolve();
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    logout: ({ commit }) => {
      Cookie.remove("token");
      commit("SET_TOKEN", "");
      commit("SET_USER", { id: "", username: "", roles: [] });
    },
    setCurrentUser: ({ commit }) => {
      return new Promise((resolve, reject) => {
        axios
          .get("/users/current")
          .then(({ data, status }) => {
            if (status === 200) {
              commit("SET_USER", data.user);
              resolve();
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};
