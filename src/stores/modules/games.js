import axios from "axios";

export default {
  state: {
    userGames: [],
    contributedGames: [],
    game: {
      id: "",
      name: "",
      description: "",
      master: {
        id: "",
        username: "",
        roles: []
      },
      players: []
    }
  },
  getters: {
    getUserGames: state => {
      return state.userGames;
    },
    getContributedGames: state => {
      return state.contributedGames;
    },
    getGame: state => {
      return state.game;
    }
  },
  mutations: {
    SET_USER_GAMES: (state, userGames) => {
      state.userGames = userGames;
    },
    ADD_USER_GAME: (state, game) => {
      state.userGames.push(game);
    },
    SET_CONTRIBUTED_GAMES: (state, contributedGames) => {
      state.contributedGames = contributedGames;
    },
    SET_GAME: (state, game) => {
      state.game.id = game.id;
      state.game.name = game.name;
      state.game.description = game.description;
      state.game.master = game.master;
    },
    SET_GAME_PLAYERS: (state, players) => {
      state.game.players = players;
    }
  },
  actions: {
    getAllUserGames: ({ commit }) => {
      return new Promise((resolve, reject) => {
        axios
          .get("user/games")
          .then(({ data, status }) => {
            if (status === 200) {
              commit("SET_USER_GAMES", data.games);
              resolve(data);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    getGamePlayers: ({ commit }, id) => {
      return new Promise((resolve, reject) => {
        axios
          .get(`games/${id}/players`)
          .then(({ data, status }) => {
            if (status === 200) {
              commit("SET_GAME_PLAYERS", data.players);
              resolve(data);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    addUserGame: ({ commit }, payload) => {
      return new Promise((resolve, reject) => {
        axios
          .post("games", payload)
          .then(({ data, status }) => {
            if (status === 201) {
              commit("ADD_USER_GAME", data.game);
              resolve(data);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};
