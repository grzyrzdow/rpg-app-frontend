import axios from "axios";

export default {
  state: {
    snackbar: {
      visible: false,
      message: "",
      timeout: 1500
    },
    sidebar: {
      visible: false
    }
  },
  getters: {
    getSnackbar: state => {
      return state.snackbar;
    },
    getSidebar: state => {
      return state.sidebar;
    }
  },
  mutations: {
    SET_SNACKBAR(state, { visible = true, message = "", timeout = 1500 }) {
      state.snackbar.visible = visible;
      state.snackbar.message = message;
      state.snackbar.timeout = timeout;
    },
    SET_SIDEBAR(state, value) {
      state.sidebar.visible = value;
    }
  },
  actions: {}
};
